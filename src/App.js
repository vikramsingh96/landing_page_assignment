import React from 'react';
import MainRouter from './MainRouter';

class App extends React.Component{

  render(){
    return(
          <React.Fragment>
              <MainRouter />
          </React.Fragment>
    );
  }
}

export default App;
