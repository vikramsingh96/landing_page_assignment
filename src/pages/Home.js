import React, { Component } from 'react';
import Navbar from '../component/Navbar';
import { eactStuff } from '../constant/EeatStuff';
import FoodCard from '../component/FoodCard';

import '../style/Home.css';
import FoodCategories from '../component/FoodCategories';

export default class Home extends Component {

    constructor(props){
        super(props);
        this.state={
            eact_Stuff:[],
            food_categories:[
                {title:'Free shipping on first order', des:'Sign up for updates and get free shipping', url:'free_shoping.svg'},
                {title:'30 minutes delivery', des:'Everything you order will be quickly delivered to your door.',url:'watch.svg'},
                {title:'Best quality guarantee', des:'We use only the best ingredients to cook the tasty fresh food for you.', url:'burger.svg'},
                {title:'Variety of dishes', des:'In our menu you’ll find a wide variety of dishes, desserts, and drinks.', url:'dishes.svg'},
                {title:'Variety of dishes',des:'In our menu you’ll find a wide variety of dishes, desserts, and drinks.', url:'dishes.svg'}
            ]
        }
    }

    componentDidMount() {
        this.setState({ eact_Stuff: eactStuff })
    }



    renderFoodCard=()=>{
        return this.state.eact_Stuff.map((value,key)=>{
            return <FoodCard title={value.title} url={value.url} key={key}/>
        })
    }

    
    

    render() {
        return (
            <React.Fragment>
                {/* -----------------------------start-section1--------------------------- */}
                <Navbar />
                <div className="section1">
                    <div className="Party-Time">Party Time!</div>
                    <div className="Group-1389">
                        <div id="left_container">
                            <img alt="img" src={require('../assets/icon/left-arrow2.png')} />
                        </div>
                        <div id="center_container">
                            <p className="Buy-any-2-burgers-and-get-15L-Pepsi-Free">Buy any 2 burgers and get 1.5L Pepsi Free</p>
                            <img alt="img" src={require('../assets/icon/vector.svg')}/>
                        </div>
                        <div id="right_container">
                            <img alt="img" src={require('../assets/icon/right-arrow.svg')} />
                        </div>
                    </div>

                    <div className="order_now">
                        <button>order now</button>
                    </div>

                    <div className="group_3090">
                        <img alt="img" src={require('../assets/img/group3090.svg')}
                            className="Group-1390-img"/>
                    </div>
                </div>

                {/* -----------------------------end-section1--------------------------- */}
                
                <section className="section2">
                    <h2>Want To Eat?</h2>
                    <img alt="img" className="brush_1" src={require('../assets/img/brush_1.svg')}/>
                    <p>Try our Most Delicious food and it usually take minutes to deliver!</p>
                    <div className="eat_stuff_container">
                        {this.renderFoodCard()}
                    </div>
                    <div className="Rectangle-274">
                        <div className="Path-4325"><img alt="img" src={require('../assets/img/group1397.svg')}/></div>
                         <div className="food_categories_container">
                             {
                                 this.state.food_categories.map((value,key)=>{
                                     return <FoodCategories key={key} title={value.title} des={value.des} url={value.url}/>
                                 })
                             }
                         </div>
                         <div className="group1419">
                             <img alt="img" src={require('../assets/img/group1419.svg')}/>
                         </div>
                    </div>
                </section>


                <section className="section3">
                    <h2>Client Testimonials</h2>
                    <img alt="img" className="brush_2" src={require('../assets/img/brush_1.svg')}/>
                    <p>Try our Most Delicious food and it usually take minutes to deliver!</p>

                    <div className="tesimonials_container">
                       <div className="tesimonial_inner_left_container">
                           <div className="Rectangle-268"></div>
                       </div>

                       <div className="tesimonial_inner_right_container">
                             <div className="text-wrapper">
                                 <img alt="img" src={require('../assets/icon/group1346.svg')} />
                                 <div className="text_p">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</div>
                                 <h3>Nina Margaret</h3>
                             </div>
                        </div>      
                    </div>         


                </section>



                
            </React.Fragment>
        )
    }
}
