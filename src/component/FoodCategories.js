import React, { Component } from 'react';
import '../style/FoodCategories.css';

export default class FoodCategories extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="food_categories_cart">
                    <img alt="img" src={require(`../assets/food_categories/${this.props.url}`)} />
                    <div className="categories_title">{this.props.title}</div>
                    <p>{this.props.des}</p>
                </div>
            </React.Fragment>
        )
    }
}
