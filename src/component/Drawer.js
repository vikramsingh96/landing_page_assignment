import React from 'react'

export default function Drawer(props) {
    return (
        <React.Fragment>
                <div className="drawer_container" style={{right: props.isOpen?'0px':'-252px'}}>
                    <li>Home</li>
                    <li>Menu</li>
                    <li>Gallery</li>
                    <li>Testiminials</li>
                    <li>Contact Us</li>
                </div>
            </React.Fragment>
    )
}
