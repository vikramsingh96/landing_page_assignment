import React, { Component } from 'react';
import Drawer from './Drawer';
import '../style/Navbar.css';

export default class Navbar extends Component {

    state={
        isOpen:false
    }

    handleDrawer=()=>{
        this.setState({isOpen:!this.state.isOpen});
    }

    render() {
        return (
            <React.Fragment>
                <div className="navbar_container">
                    <div className="Rectangle-287">
                        <div className="categories_box">
                            <li className="categories">Home</li>
                            <li className="categories">Menu</li>
                            <li className="categories">Gallery</li>
                            <li className="categories">Testiminials</li>
                            <li className="categories">Contact Us</li>
                        </div>
                        {/* <li> */}
                            <div className="Rectangle-288">
                                <li className="categories_action"><img alt="img" src={require('../assets/icon/search_icon.svg')}/></li>
                                <li className="categories_action"><img alt="img" src={require('../assets/icon/user_icon.svg')}/></li>
                                <li className="categories_action"><img alt="img" src={require('../assets/icon/cart_icon.svg')}/></li>
                            </div>
                        {/* </li> */}
                    </div>
                </div>

                <div className="brand_name_container">
                    <img src={require('../assets/icon/brand_icon.svg')} />
                </div>    


                {/* ------------------------------------mobile-navbar--------------------------------------- */}
                
                <div className="mobile_navbar">
                    <div className="mobile_brand_wrapper">
                        <img alt="img" src={require('../assets/icon/brand_icon.svg')}/>
                    </div>
                    <div className="mobile_categories">
                        <li><img alt="img" src={require('../assets/icon/search_icon.svg')}/></li>
                        <li><img alt="img" src={require('../assets/icon/user_icon.svg')}/></li>
                        <li><img alt="img" src={require('../assets/icon/cart_icon.svg')}/></li>
                        <li onClick={this.handleDrawer}><img  alt="img" src={require('../assets/icon/menuIcon.png')} style={{width:40}}/></li>
                    </div>
                </div>
                <Drawer isOpen={this.state.isOpen}/>
            </React.Fragment>
        )
    }
}
