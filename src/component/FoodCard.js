import React, { Component } from 'react';
import '../style/FoodCard.css';

export default class FoodCard extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="food_card_container">
                    <img alt="img" src={require(`../assets/eat_stuff/${this.props.url}`)} />
                    <p>{this.props.title}</p>
                </div>
            </React.Fragment>
        )
    }
}
