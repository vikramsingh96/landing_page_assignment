import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";

  import Home from './pages/Home';
  
  
  export default class MainRouter extends Component {
      render() {
          return (
            <Router>
              <Switch>
                <Route exact path="/" component={Home}/>
              </Switch>
            </Router>
          )
      }
  }
  